const request = require('supertest');
const { expect } = require('chai');

describe('User API', () => {
  it('create a new user, update the user, get user list, and then delete the user', async () => {
    // Create a new user
    const user = { name: 'John Doe', job: 'Software Engineer' };
    const createUserResponse = await request('https://reqres.in/api')
      .post('/users')
      .send(user);

    console.log('Create User Response Body:', createUserResponse.body);
    console.log('Create User Response ID:', createUserResponse.body.id);

    expect(createUserResponse.status).to.equal(201);
    expect(createUserResponse.body).to.have.property('name', user.name);
    expect(createUserResponse.body).to.have.property('job', user.job);
    expect(createUserResponse.body).to.have.property('id');
    expect(createUserResponse.body).to.have.property('createdAt');

    // Update the user
    const id = createUserResponse.body.id; // Retrieve the ID of the previously created user
    const updatedUser = { name: 'Jane Smith', job: 'Product Manager' };
    const updateUserResponse = await request('https://reqres.in/api')
      .put(`/users/${id}`)
      .send(updatedUser);

    console.log('Update User Response Body:', updateUserResponse.body);

    expect(updateUserResponse.status).to.equal(200);
    expect(updateUserResponse.body).to.have.property('name', updatedUser.name);
    expect(updateUserResponse.body).to.have.property('job', updatedUser.job);

    // Get user list
    const page = 2;
    const getUsersResponse = await request('https://reqres.in/api')
      .get(`/users?page=${page}`);

    console.log('Get Users Response Body:', getUsersResponse.body);

    expect(getUsersResponse.status).to.equal(200);
    expect(getUsersResponse.body).to.have.property('page', page);
    expect(getUsersResponse.body).to.have.property('data').that.is.an('array');

    // Delete the user
    const deleteUserResponse = await request('https://reqres.in/api')
      .delete(`/users/${id}`);

    console.log('Delete User Response Body:', deleteUserResponse.body);

    expect(deleteUserResponse.status).to.equal(204);


  });
});
